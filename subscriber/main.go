package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/appdev.ananrafs1/go-rabbitmq/aggregator"
	"gitlab.com/appdev.ananrafs1/go-rabbitmq/helper"
)

func main() {

	conn, err := helper.CreateConnection()
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	subscriber := aggregator.Subscriber{
		Queue: aggregator.Queue{
			Name:      "q1",
			Exclusive: true,
		},
		Exchange: aggregator.Exchange{
			Name:         "rmq-1",
			ExchangeType: aggregator.Fanout,
			Durable:      true,
		},
	}
	err = subscriber.Initialized(conn)
	if err != nil {
		panic(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(20*time.Second))
	defer cancel()

	err = subscriber.Notified(ctx, func(msg []byte) {
		log.Printf(" [x] %s \n", string(msg))

	}, aggregator.NotificationOption{
		NoAck: true,
	})
	if err != nil {
		panic(err)
	}
}
