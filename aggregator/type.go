package aggregator

import "github.com/streadway/amqp"

type ExchangeType int

const (
	Direct ExchangeType = iota + 1
	Topic
	Fanout
	Headers
	Default
	DeadLetter
)

func (excType *ExchangeType) String() string {
	return []string{"direct", "topic", "fanout", "headers", "", "dead letter"}[*excType-1]
}

type Exchange struct {
	Name         string
	ExchangeType ExchangeType
	Durable      bool
	AutoDelete   bool
	Internal     bool
	NoWait       bool
	Arguments    amqp.Table
}
type Queue struct {
	Name       string
	Durable    bool
	Exclusive  bool
	AutoDelete bool
	NoWait     bool
	Arguments  amqp.Table
}
