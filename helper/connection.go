package helper

import (
	"fmt"

	"github.com/streadway/amqp"
)

type connector struct {
	username string
	pw       string
	uri      string
}

func (c *connector) defineUri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/", c.username, c.pw, c.uri)
}

func CreateConnection() (*amqp.Connection, error) {
	fmt.Println("queue preparing")
	amqpUri := connector{
		username: "guest",
		pw:       "guest",
		uri:      "localhost:5672",
	}
	conn, err := amqp.Dial(amqpUri.defineUri())
	if err != nil {
		return nil, err
	}
	fmt.Println("Connected")
	return conn, nil
}
