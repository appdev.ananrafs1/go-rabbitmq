package aggregator

import (
	"context"
	"github.com/streadway/amqp"
)

type Publisher struct {
	Exchange Exchange
	Routing  string
	channel  *amqp.Channel
}

type Subscriber struct {
	Exchange Exchange
	Queue    Queue
	Routing  string
	channel  *amqp.Channel
}

func (p *Publisher) Initialized(conn *amqp.Connection) (err error) {
	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	p.channel = ch
	err = ch.ExchangeDeclare(
		p.Exchange.Name,
		p.Exchange.ExchangeType.String(),
		p.Exchange.Durable,
		p.Exchange.AutoDelete,
		p.Exchange.Internal,
		p.Exchange.NoWait,
		p.Exchange.Arguments,
	)

	return
}

func (s *Subscriber) Initialized(conn *amqp.Connection) (err error) {
	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	s.channel = ch

	err = ch.ExchangeDeclare(
		s.Exchange.Name,
		s.Exchange.ExchangeType.String(),
		s.Exchange.Durable,
		s.Exchange.AutoDelete,
		s.Exchange.Internal,
		s.Exchange.NoWait,
		s.Exchange.Arguments,
	)

	return
}

func (p *Publisher) Close() {
	p.channel.Close()
}

func (s *Subscriber) Close() {
	s.channel.Close()
}

func (p *Publisher) Notify(obj []byte, option NotifyOption) (err error) {
	if p.channel == nil {
		panic("error: Not Initialized \n")
	}
	data := amqp.Publishing{
		ContentType: "text/plan",
		Body:        []byte(obj),
	}
	err = p.channel.Publish(
		p.Exchange.Name, p.Routing, option.Mandatory, option.Immediate, data,
	)
	return
}

func (s *Subscriber) Notified(ctx context.Context, handler func(msg []byte), option NotificationOption) (err error) {
	if s.channel == nil {
		panic("error: Not Initialized \n")
	}

	q, err := s.channel.QueueDeclare(
		s.Queue.Name, s.Queue.Durable, s.Queue.AutoDelete, s.Queue.Exclusive, s.Queue.NoWait, s.Queue.Arguments,
	)
	if err != nil {
		return
	}
	err = s.channel.QueueBind(
		q.Name, s.Routing, s.Exchange.Name, s.Queue.NoWait, s.Queue.Arguments,
	)
	if err != nil {
		return
	}

	msgs, err := s.channel.Consume(
		s.Queue.Name, option.ConsumerTag, option.NoAck, option.Exclusive, option.NoLocal, option.NoWait, option.Arguments,
	)
	if err != nil {
		return
	}

	for {
		select {
		case d := <-msgs:
			handler((&d).Body)
		case <-ctx.Done():
			return
		}
	}

}
