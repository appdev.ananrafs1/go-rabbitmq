package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/appdev.ananrafs1/go-rabbitmq/aggregator"
	"gitlab.com/appdev.ananrafs1/go-rabbitmq/helper"
)

var (
	interval int
	text     string
)

func init() {
	flag.StringVar(&text, "message", "default coming", "body content")
	flag.IntVar(&interval, "interval", 5, "interval")
	flag.Parse()
	fmt.Printf("Content Flag : msg : %s, interval: %d \n", text, interval)
}
func main() {

	conn, err := helper.CreateConnection()
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	publisher := aggregator.Publisher{
		Exchange: aggregator.Exchange{
			Name:         "rmq-1",
			ExchangeType: aggregator.Fanout,
			Durable:      true,
			AutoDelete:   false,
			Internal:     false,
			NoWait:       false,
			Arguments:    nil,
		},
		Routing: "",
	}
	defer publisher.Close()
	err = publisher.Initialized(conn)
	if err != nil {
		panic(err)
	}
	for {
		err = publisher.Notify([]byte(text), aggregator.NotifyOption{
			Mandatory: false,
			Immediate: false,
		})
		if err != nil {
			panic(err)
		}

		time.Sleep(time.Duration(interval) * time.Second)
	}
}
