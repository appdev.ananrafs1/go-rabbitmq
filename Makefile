.DEFAULT_GOAL := run

message = ulala
interval = 2

addrmq: cleanup
	@docker run -d --hostname my-rabbit --name rabbittank -p 15672:15672 -p 5672:5672 rabbitmq:3-management

cleanup:
	@docker container rm -f rabbittank

run : 
	@go run ./publisher/main.go -message="$(message)" -interval=$(interval)
	
run_sub:
	@go run ./subscriber/main.go