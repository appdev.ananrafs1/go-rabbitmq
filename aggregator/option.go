package aggregator

import "github.com/streadway/amqp"

type NotifyOption struct {
	Mandatory bool
	Immediate bool
}

type NotificationOption struct {
	ConsumerTag string
	NoAck       bool
	Exclusive   bool
	NoLocal     bool
	NoWait      bool
	Arguments   amqp.Table
}